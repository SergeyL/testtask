from rest_framework import serializers

from web.models import News

class NewsSerializer(serializers.ModelSerializer):
    date_publish = serializers.DateField(format='%d.%m.%Y')

    class Meta:
        model = News
        exclude = ('id',)
