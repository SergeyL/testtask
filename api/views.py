from django.shortcuts import render
from rest_framework import viewsets
from web.models import News

from .serializers import NewsSerializer


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by('-date_publish')
    serializer_class = NewsSerializer
