from django.db import models

class News(models.Model):
    """
    новости
    """
    title = models.CharField(verbose_name='Заголовок', max_length=256)
    date_publish = models.DateField(verbose_name='Дата публикации')
    text = models.TextField(verbose_name='Содержание новости')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
