from django.contrib import admin
from .models import News

@admin.register(News)

class IncidentAdmin(admin.ModelAdmin):
    def date_publish_formated(self, obj):
        return obj.date_publish.strftime('%d.%m.%Y')

    date_publish_formated.short_description = 'Дата публикации'

    list_display = ('id','title', 'date_publish_formated')
    list_filter = ('date_publish',) #  TODO: localization interface?
    #  TODO: try add search for date_publish_formated
    search_fields = ['id','title', 'date_publish']
    ordering = ('-date_publish',)
